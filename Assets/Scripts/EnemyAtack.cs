﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class EnemyAtack : MonoBehaviour{
    [SerializeField] private GameObject m_Player;

    [FormerlySerializedAs("shootPosition")] [SerializeField]
    Transform shootPoint;

    [SerializeField] GameObject bullet;
    [SerializeField] float      fireRate = 2f;
    // [SerializeField] private Text _HitPointEnemy;
    

    private Vector2   direction;
    private Coroutine _coroutine;
    private bool isBulletRunnig = false;
    private float dist;
    
    void Update() {
        MoveToPlayer();
        RotatePlayer();
        // ShootToPlayer();
        CheckHit();
        CheckDistance();

    }

    private void CheckDistance() {
        dist = Vector2.Distance(m_Player.transform.position, transform.position);
   }

    private void CheckHit() {
        Vector2      _shooting = shootPoint.position;
        RaycastHit2D rayhit    = Physics2D.Raycast(_shooting, direction, 5f);
       
        if(rayhit.collider == null) {
            return;
        }

        if(!rayhit.collider.CompareTag("player")) {
            return;
        }

        if(isBulletRunnig) {
            return;
        }

        StartCoroutine(ShootingToPlayer());
        isBulletRunnig = true;
    }

    // private void ShootToPlayer() {
    //     Instantiate(bullet, shootPosition.position, transform.rotation);
    // }

    IEnumerator ShootingToPlayer() {
        Instantiate(bullet, shootPoint.position, transform.rotation);
        yield return new WaitForSeconds(3);
        isBulletRunnig = false;
    }

    private void RotatePlayer() {
        Vector2 playerPos = m_Player.transform.position;
        direction    = playerPos - (Vector2) transform.position;
        transform.up = direction;
    }

    private void MoveToPlayer() {

        if(dist < 5f) {
        transform.position = Vector3.MoveTowards(transform.position, m_Player.transform.position,  0.02f);
            
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.transform.gameObject.CompareTag("laserplayer")) {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
