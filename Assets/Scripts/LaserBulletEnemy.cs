﻿using UnityEngine;

public class LaserBulletEnemy : MonoBehaviour{
    [SerializeField] float speed = 4;

    void Start() {
        GetComponent<Rigidbody2D>().velocity = transform.up * speed;
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
