﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Slider healthBar;
    [SerializeField] GameObject menu;

    PlayerController player;
    private bool paused = false;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        healthBar.maxValue = player.GetHealth();
    }

    // Update is called once per frame
    void Update()
    {
        // Paused();
        healthBar.value = player.GetHealth();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            menu.SetActive(!menu.activeSelf);
            StartCoroutine(Pause());
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
               {
                
                   Time.timeScale = 1;
               }
    }

    private IEnumerator Pause() {
        yield return new WaitForSeconds(1);
        Time.timeScale = 0;
    }
    
    private IEnumerator NoPause() {
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 1;
    }

    
    private IEnumerator Paused() {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            if (!paused) {
                yield return new WaitForSeconds(1);
                Time.timeScale = 0;
                paused         = true;
              
            } else {
                yield return new WaitForSeconds(0.2f);
                Time.timeScale = 1;
                paused         = false;
               
            }
        }
       
    }
    
}
