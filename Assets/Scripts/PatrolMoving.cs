﻿
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public enum StateEnemy{
    FIND_POINT,
    MOVE_TO,
    WAIT
}
public class PatrolMoving : MonoBehaviour{
    [SerializeField] private Transform[] patrolPoints;
    private                  float       speed = 5f;
    public StateEnemy CurrentState {get; set;}
    private int nextPatrolPointIndex;
    private int currientPoint;
    private float distance;
    
    void Start() {
    }

    void Update() {
        // MoveToPointEnemy();
        switch(CurrentState) {
            
            case StateEnemy.FIND_POINT :
                print("FIND_POINT");
                FindNextPoint();
                
                CurrentState = StateEnemy.MOVE_TO;
                break;
            case StateEnemy.MOVE_TO :
                print("MoveToPoint");
                MoveToPoint();
     
                break;
            case StateEnemy.WAIT : 
                
                break;
       
        }
    }

    private void FindNextPoint() {
        currientPoint = Random.Range(0, patrolPoints.Length);
    }

    private void MoveToPoint() {
        
        distance = Vector2.Distance(transform.position, patrolPoints[currientPoint].position);
        if(distance > 0.2f) {
            transform.position = Vector3.MoveTowards(transform.position, patrolPoints[currientPoint].position, speed 
            * Time.deltaTime);
        }
        else {
            
            CurrentState = StateEnemy.FIND_POINT;    
        }

        

    }
    
    private IEnumerator MoveToPointEnemy() {
        while(true) {
            transform.position = Vector3.MoveTowards(transform.position, patrolPoints[nextPatrolPointIndex].position, speed);
            yield return null;
            yield return new WaitForSeconds(2f);
            CurrentState = StateEnemy.FIND_POINT;    
        }
    }

    private void Patrol() {
        int currientPoint = Random.Range(0, patrolPoints.Length);
        if(nextPatrolPointIndex != currientPoint) {
            StartCoroutine(MoveToPointEnemy());
            CurrentState = StateEnemy.WAIT;
        }

        nextPatrolPointIndex = currientPoint;
    }
}
